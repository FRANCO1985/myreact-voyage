import React from 'react';
import {useState, useEffect} from 'react';

//Voir doc react https://fr.reactjs.org/docs/faq-ajax.html
const Countries = (props) => {

    const [countriesList, setCountries] = useState([]);
    const [error, setError] = useState(null);

    useEffect(() => {
        fetch("https://restcountries.eu/rest/v2/")
        .then(res => res.json())
        .then(
            (result) => {
                setCountries(result); //attribue le resultat dans la variable items (pour la réutiliser après)
            },
            (error) => {
               setError(error);
            }
        );
    }, []);

    if(error){
        return (<div>Erreur lors de l'appel à l'API {error.message}</div>);
    }
    else {
        return (
            <>
                <select id={props.id} className={props.className} name={props.name} onChange={props.onChange} >
                <option>{props.textPays}</option>
                {countriesList.map(country => (
                    <option value={country.alpha3Code} key={country.alpha3Code}>{country.name}</option>
                ))}
                </select>
            </>
        );
    }
}
export default Countries; 

