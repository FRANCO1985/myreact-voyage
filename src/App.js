import React, { Component } from 'react';
import './App.css';
import Countdown from './Countdown';




class App extends Component {
  render() {
    const currentDate = new Date();
    const year = (currentDate.getMonth() === 11 && currentDate.getDate() > 23) ? currentDate.getFullYear() + 1 : currentDate.getFullYear();
    return (
      <div className="App">
      <Countdown date= {`${year}-07-13T00:00:00`} />
        
      </div>
    );
  }
}

export default App;
