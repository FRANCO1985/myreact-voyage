import React from 'react';
import App from './App';


const Home = () => {
    return (
        <div className="Home">
            <App/>
            <div className="d-flex justify-content-center text-warning">
                <h2>Your Trip Starts Here</h2>
            </div>

        </div>


    )
}

export default Home;