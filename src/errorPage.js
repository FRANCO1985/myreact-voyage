import React from 'react'

const errorPage = () => {
    return (
        <div className="errorPage">
            <div className="container">
              <h1>Oops Error Page 404</h1>
              <h3>Not Found</h3> 
            </div>
        </div>
    )
}

export default errorPage