import React from 'react'



function About() {
    return (
    
        <div className="container col-sm-6">
            <h5 className="mt-5 d-flex justify-content-center text-white">
                Qui nous sommes:

                Depuis ses modestes débuts en France, Voyage Frank s'est développé avec son concept unique de vente,
                avec des bureaux en France, au Royaume-Uni, en Italie, en Espagne, en Allemagne, en Suisse, en Belgique et aux Pays-Bas.

                Notre équipe
                Nous sommes une petite équipe d'experts en voyages bien définie. Nos éditeurs achètent certains des meilleurs hôtels et complexes hôteliers du monde,
                 nos négociateurs garantissent des offres exclusives et nos contrôleurs de prix s'assurent que vous obtenez la meilleure offre possible.

                Ce que nous faisons
                Chaque semaine, vous recevrez des newsletters de notre part, contenant nos meilleures offres et les plus inspirantes de la semaine.
                Nos offres (ventes flash) sont en direct jusqu'à 14 jours, mais vous trouverez toujours une sélection de séjours en ville chic, 
                de belles escapades à la campagne et de magnifiques retraites sur la plage.

            </h5>
        </div>
    )
}

export default About
