import React from 'react';

function Voyageur(props) {

   

    const handleDelete = event => {
        event.preventDefault()
        if (props.onDelete) {
            props.onDelete()
        }
    }

    return (

        <div>
            <input className="name ml-2" name="name" placeholder="Nom" type="text" onChange={props.onChange} required />
            <input className="family ml-2" name="prenom" placeholder="Prenom" type="text" onChange={props.onChange} required />  
            <input className="date ml-2 mr-2 mb-2" name="birthday" placeholder="" type="date" onChange={props.onChange} required />
            <button className="btn btn-danger" onClick={handleDelete}>Delete</button>     
        </div>
         
    );
}

export default Voyageur;





