import React, { useState } from 'react';
import Voyageur from './Voyageur';
import Countries from './Countries';
import App from './App';


const Form = () => {

    let today = new Date().toISOString().slice(0, 10);


    const [voyageurLastId, setVoyageurLastId] = useState({});
    const [listeVoyageurs, setListeVoyageurs] = useState([]);
    const [voyageType, setVoyageType] = useState(true);
    const [departure, setDeparture] = useState(today);
    const [errors, setErrors] = useState({});
    const [values, setValues] = useState({});


    const getNextId = () => {
        const id = voyageurLastId;
        setVoyageurLastId(id + 1)
        return id;
    }

    const handleAddVoyageur = event => {
        event.preventDefault();
        const nouveauVoyageur = {
            id: getNextId(),
        };
        const nouveauListeVoyageurs = [...listeVoyageurs];
        nouveauListeVoyageurs.push(nouveauVoyageur);
        setListeVoyageurs(nouveauListeVoyageurs);
    }

    const handleDeleteVoyageur = i => () => {
        const voyageursCopy = [...listeVoyageurs]
        console.log("delete", i)
        voyageursCopy.splice(i, 1)
        setListeVoyageurs(voyageursCopy)
    }
    //On "gère" le changement : dès qu'on saisie un caractère dans un champ on passe par cette fonction
    const handleChange = (event) => {
        if (event.target.name === 'date_depart') setDeparture(event.target.value)
        event.persist();
       

        //On met à jour la valeur de values pour récupérer les valeurs et les traiter
        setValues(values => ({ ...values, [event.target.name]: [event.target.value] }));
        
    }


    const voyageursDiv = listeVoyageurs.map((voyageur, i) => {
        return <Voyageur key={voyageur.id} onDelete={handleDeleteVoyageur(i)} onChange={handleChange} />
    })

    const handleSubmit = (event) => {

        let errors = {};

        //L'action par défaut (le submit) ne doit pas être prise en compte
        if (event) event.preventDefault();

        //Test si la depart est vide
        if (!values.depart) {
            errors.depart = 'Erreur veuillez vérifier le depart ';
        }

        //Test si le voyage de retour est vide 
        if (!values.retour) {
            errors.retour = 'Erreur veuillez vérifier le retour';
        }

        //Test si le nombre de voyageur est vide 
        if (!values.date_depart) {
            errors.date_depart = 'Erreur veuillez vérifier le date de depart';
        }

        //Test si le date retour est vide
        if (!values.date_retour) {
            errors.date_retour = 'Erreur veuillez vérifier le retour';
        }

        //Test si le lenght de la listeVoyageur est vide
        if (!listeVoyageurs.length) {
            errors.voyageurs = 'Erreur veuillez verifier le nombre de voyageurs'
        }


        /*listeVoyageurs.map((voyageur, i) => {
            if (!voyageur.name){
                errors.name = `Erreur veuillez vérifier le name ${voyageur.id}`
                console.log(errors)
            }
            if (!voyageur.prenom) {
                errors.prenomVoyageur = 'Erreur veuillez vérifier le prenom';
            }
            if (!voyageur.birthday) {
                errors.birthdayVoyageur = 'Erreur veuillez vérifier le datenaissancce';
            }
            return errors;
        })*/

        //On met à jour la valeurs de errors
        //pour rendre nos erreurs accessibles dans le formulaire pour l'affichage dans les spans
        setErrors(errors);

        //Si notre objet ne contient pas d'erreur on peut le valider 
        if (Object.keys(errors).length === 0) {
            alert('Le formulaire est validé, les données sont bien envoyés');
        }

    }

    return (
        <div id="form" className="container">
            <App/>
            <div className="form-check mt-5">
                <label htmlFor="aller-retour">
                    <input defaultChecked={voyageType ? true : false} id="trip" type="radio" name="aller" onClick={() => { setVoyageType(true) }} />
                    <span>Aller-Retour</span>
                </label>
                <label htmlFor="aller">
                    <input id="trip" type="radio" name="aller" onClick={() => { setVoyageType(false) }} />
                    <span>Aller simple</span>
                </label>
            </div>
            <form onSubmit={handleSubmit} method="POST">
                <div className="row mt-5">

                    <div className="form-group col-sm-6">
                        <label htmlFor="depart"></label>
                        <Countries name="depart" id="depart" textPays="Depart" className="form-control" onChange={handleChange} />
                        <span className="error">{errors.depart}</span>
                    </div>
                    <div className="form-group col-sm-6">
                        <label htmlFor="retour"></label>
                        <Countries name="retour" id="retour" textPays="Retour" className="form-control" onChange={handleChange} />
                        <span className="error">{errors.retour}</span>
                    </div>
                </div>
                <div className="row">
                    <div className="form-group col-sm-6">
                        <label htmlFor="date-depart">Date Depart</label>
                        <input min={today} value={values.date_depart} type="date" name="date_depart" id="dateDepart" className="form-control" onChange={handleChange} />
                        <span className="error">{errors.date_depart}</span>
                    </div>
                    {voyageType ?

                        <div className="form-group col-sm-6">
                            <label htmlFor="date-retour">Date Retour</label>
                            <input min={departure} value={values.date_retour} type="date" name="date_retour" id="dateRetour" className="form-control" onChange={handleChange} />
                            <span className="error">{errors.date_retour}</span>
                        </div> : null

                    }

                </div>
                <div className="form-group">
                    <label htmlFor="voyageurs"> Nombre de Voyageurs</label>
                    <div className="input-group">
                        <input type="text" id="text" name="voyageurs" className="form-control" value={listeVoyageurs.length} onChange={handleChange} />
                        <span className="error">{errors.voyageurs}</span>
                        <button className="input-group-text" id="addUser" onClick={handleAddVoyageur}>Add Voyageur</button>


                    </div>
                </div>
                <div id="voyageur">
                    {voyageursDiv}
                    
                </div>
                <button type="submit" className="btn btn-success" value="valider">Valider</button>
            </form>
        </div>

    );
}

export default Form;


