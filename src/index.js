import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch, } from 'react-router-dom';
import './index.css';
import Home from './Home';
import Form from './Form';
import About from './About';
import Header from './Header';
import errorPage from './errorPage';
import * as serviceWorker from './serviceWorker';


ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Header/>
      <Switch>
        <Route exact path="/home"component={Home} />
        <Route path="/form" component={Form} />
        <Route path="/about" component={About} />
        <Route component = {errorPage} />
      </Switch>

    </Router>
  </React.StrictMode>,
  document.getElementById('root')
  );




// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();



