import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';


function Header() {
  return (

    <div className="Header">
      <Navbar collapseOnSelect expand="lg" variant="dark">
        <Navbar.Brand className="text-white" href="Home">Home</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link className="text-white"href="Form">Join your trip</Nav.Link>
          </Nav>
          <Nav>
            <Nav.Link className="text-white" href="About">About Us</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}

export default Header;
